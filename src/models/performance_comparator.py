class PerformanceComparator:
    _source_report = None
    _target_report = None
    _data = []

    def __init__(self, source_report: list, target_report: list):
        self._source_report = source_report
        self._target_report = target_report

    def generate(self):
        for el in self._source_report:
            performance_metrics = {
                "subject": el["subject"],
                "metrics": []
            }

            target_metrics = \
                self._get_target_metrics_for_subject(el["subject"])
            if target_metrics is None:
                self._data.append(performance_metrics)
                continue
            for metric in el["metrics"]:
                target_metric = self._get_metric_from_target_metrics(
                    target_metrics, metric["name"]
                )

                diff_metric = {
                    "name": metric["name"],
                    "source_branch_value": metric["value"],
                    "target_branch_value": target_metric["value"],
                    "source_branch_better": self.is_source_branch_better(
                        metric["value"],
                        target_metric["value"],
                        metric["desiredSize"]
                    )
                }

                performance_metrics["metrics"].append(diff_metric)

            self._data.append(performance_metrics)

        return self._data

    def _get_target_metrics_for_subject(self, subject_name):
        for key in self._target_report:
            if key["subject"] == subject_name:
                return key["metrics"]

        return None

    def _get_metric_from_target_metrics(self, metrics, metric_name):
        for metric in metrics:
            if metric["name"] == metric_name:
                return metric

        return None

    def is_source_branch_better(self, source_branch_value,
                                target_branch_value,
                                operator):
        switcher = {
            "smaller": source_branch_value <= target_branch_value,
            "larger": source_branch_value >= target_branch_value
        }

        return switcher.get(operator, None)
