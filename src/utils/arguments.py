import argparse
import os


class Arguments:
    _parser = ''
    _arguments = ''

    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Gitlab Performance Reporter'
        )

        # Add OS.environ default parameters
        parser.add_argument('--source',
                            dest='source',
                            default=os.getenv('CI_COMMIT_BRANCH'),
                            help='The source branch to compare against')

        parser.add_argument('--project-id',
                            dest='project_id',
                            default=os.getenv('CI_PROJECT_ID'),
                            help='The Gitlab project ID')

        parser.add_argument('--token',
                            dest='access_token',
                            required=True,
                            help='The Gitlab Personal Access Token')

        parser.add_argument('--job',
                            dest='job',
                            default='performance',
                            help='The name of the job in which the "performance.json" is being generated. Defaults to "performance"')  # noqa: E501

        parser.add_argument('--template',
                            dest='template',
                            default='classic.md',
                            help='The filename of the template to use (ex. classic.md)')  # noqa: E501

        self._parser = parser
        self._arguments = self._parser.parse_args()

    def get_arguments(self):
        return self._arguments
