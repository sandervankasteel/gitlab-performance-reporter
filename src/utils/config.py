class Config:
    _parsed_args = None

    instance_url = 'https://gitlab.com/api/v4'
    project_id = None
    source_branch = None
    token = None
    template = None
    job_name = None
    _cwd = None

    def __init__(self, args, root_path):
        self._parsed_args = args.get_arguments()

        self.source_branch = self._parsed_args.source
        self.project_id = self._parsed_args.project_id
        self.token = self._parsed_args.access_token
        self.template = self._parsed_args.template
        self.job_name = self._parsed_args.job
        self._cwd = root_path
