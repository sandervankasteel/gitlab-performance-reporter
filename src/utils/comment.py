from jinja2 import Template
from utils.config import Config


class Comment:
    _template = None
    _merge_request = None
    _performance_report = None

    def __init__(self, config: Config, merge_request, performance_report):
        template_location = config._cwd + '/templates/' + config.template

        self._template = Template(open(template_location).read())
        self._merge_request = merge_request
        self._performance_report = performance_report

    def render(self):
        parameters = self._generate_parameters()
        return self._template.render(parameters)

    def _generate_parameters(self):
        return {
            'target_branch': self._merge_request['target_branch'],
            'source_branch': self._merge_request['source_branch'],
            'report':  self._performance_report
        }
