import os
from api.gitlab.performance_artifact import PerformanceArtifact
from api.gitlab.merge_request import MergeRequest
from api.gitlab.note import Note
from models.performance_comparator import PerformanceComparator
from utils.arguments import Arguments
from utils.comment import Comment
from utils.config import Config

if __name__ == "__main__":
    args = Arguments()
    root_path = os.path.dirname(os.path.abspath(__file__))
    c = Config(args, root_path)

    mr = MergeRequest(c)
    merge_requests = mr.get_merge_requests()

    for merge_request in merge_requests:
        merge_request_id = merge_request['iid']

        performance_artifacts = \
            PerformanceArtifact(config=c,
                                branch=merge_request['source_branch'])
        source_performance = performance_artifacts.download_and_parse()

        performance_artifacts.set_branch(merge_request['target_branch'])
        target_performance = performance_artifacts.download_and_parse()

        if target_performance is not None and source_performance is not None:
            pc = PerformanceComparator(source_report=source_performance,
                                       target_report=target_performance
                                       )
            report = pc.generate()

            comment = Comment(config=c, merge_request=merge_request,
                              performance_report=report)
            rendered_comment = comment.render()
        else:
            rendered_comment = "### Gitlab Performance Reporter" \
                               "\n\n Nothing to compare against."

        n = Note(config=c)
        note_response = n.create_or_update_note(merge_request_id,
                                                body=rendered_comment)
