from api.gitlab.entity import Entity
from api.gitlab.gitlab import Gitlab
from utils.config import Config
from utils.string import String


class PerformanceArtifact(Entity):
    _branch = None
    _file_name = 'performance.json'

    def __init__(self, config: Config, branch):
        super().__init__(config)
        self._branch = branch
        self._endpoint = '/projects/:id/jobs/artifacts/:ref_name/raw/:artifact_path?job=:name'  # noqa: E501

    def download_and_parse(self):
        replacements = {
            ":id": str(self._configuration.project_id),
            ":ref_name": self._branch,
            ":artifact_path": self._file_name,
            ":name": self._configuration.job_name
        }

        url = String.replace(self._endpoint, replacements)

        result = Gitlab.make_request(
            token=self._configuration.token,
            url=url,
            method='get'
        )

        if 'message' in result and "404" in result['message']:
            return None

        return result

    def set_branch(self, branch):
        self._branch = branch
