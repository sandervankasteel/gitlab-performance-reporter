from api.gitlab.entity import Entity
from api.gitlab.gitlab import Gitlab
from utils.config import Config
from utils.string import String


class Note(Entity):

    def __init__(self, config: Config):
        super().__init__(config)
        self._endpoint = '/projects/:id/merge_requests/:merge_request_iid/notes'  # noqa: E501

    def create_or_update_note(self, mr_id, body):
        note = self._get_performance_reporter_note_for_mr(mr_id)

        if note is None:
            return self._create_note(mr_id, body)
        else:
            return self._update_note(mr_id, body, note['id'])

    def _create_note(self, mr_id, body):
        replacements = {
            ":id": self._configuration.project_id,
            ":merge_request_iid": str(mr_id)
        }

        url = String.replace(self._endpoint, replacements)

        params = {
            'body': body
        }

        return Gitlab.make_request(
            token=self._configuration.token,
            url=url,
            method='post',
            params=params
        )

    def _update_note(self, mr_id, body, note_id):
        url = '/projects/:id/merge_requests/:merge_request_iid/notes/:note_id'
        url = String.replace(url, {
            ':id': str(self._configuration.project_id),
            ':merge_request_iid': str(mr_id),
            ':note_id': str(note_id)
        })

        params = {
            'body': body
        }

        return Gitlab.make_request(
            self._configuration.token,
            url=url,
            method='put',
            params=params
        )

    def _get_performance_reporter_note_for_mr(self, mr_id):
        url = '/projects/:id/merge_requests/:merge_request_iid/notes'
        url = String.replace(url, {
            ':id': str(self._configuration.project_id),
            ':merge_request_iid': str(mr_id)
        })

        notes = Gitlab.make_request(
            self._configuration.token,
            url=url,
            method='get'
        )

        for note in notes:
            if '### Gitlab Performance Reporter' in note['body']:
                return note

        return None
