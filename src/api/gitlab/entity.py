from utils.config import Config


class Entity:
    _endpoint = None
    _configuration = None

    def __init__(self, configuration: Config):
        self._endpoint = '/'
        self._configuration = configuration

    def get_endpoint(self):
        return self._endpoint
