import requests

from utils.config import Config


class Gitlab:
    _instance_url = ''
    _token = ''
    _project_id = ''

    def __init__(self, token,
                 project_id,
                 instance_url='https://gitlab.com/api/v4'):
        self._instance_url = instance_url
        self._token = token
        self._project_id = project_id

    def get_token(self):
        return self._token

    def get_project_id(self):
        return self._project_id

    @staticmethod
    def make_request(token, url, method,
                     params: dict = {},
                     payload: dict = {}):
        headers = {
            'PRIVATE-TOKEN': token
        }

        url = Config.instance_url + url

        response = requests.request(method, url,
                                    headers=headers,
                                    params=params)
        return response.json()
