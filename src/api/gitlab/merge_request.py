from api.gitlab.entity import Entity
from api.gitlab.gitlab import Gitlab
from utils.config import Config


class MergeRequest(Entity):

    def __init__(self, configuration: Config):
        super().__init__(configuration)

        self._endpoint = '/merge_requests?state=open&target_branch='
        self._endpoint_params = {
            'state': 'opened',
            'source_branch': self._configuration.source_branch
        }

    def get_merge_requests(self):
        return Gitlab.make_request(
            token=self._configuration.token,
            url=self._endpoint,
            method='get',
            params=self._endpoint_params
        )
