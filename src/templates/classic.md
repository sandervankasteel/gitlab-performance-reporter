### Gitlab Performance Reporter

Comparing `{{ source_branch }}` vs `{{ target_branch }}`

{% for metrics in report %}
**Subject:** `{{ metrics.subject }}`

Status | Description | Comparison
--- | --- | ---
{% if metrics.metrics|length == 0 -%}
❔ | `{{ metrics.subject }}` has nothing to compare against | 
{%- endif -%}

{% for metric in metrics.metrics -%}
{% if metric.source_branch_better -%}
✅ | {{ metric.name }} | current value {{ metric.source_branch_value }} vs {{ metric.target_branch_value }}
{% else -%}
❌ | {{ metric.name }} | current value {{ metric.source_branch_value }} vs {{ metric.target_branch_value }}
{% endif %}
{%- endfor %}


{% endfor %}