# Gitlab Performance Reporter

[![pipeline status](https://gitlab.com/sandervankasteel/gitlab-performance-reporter/badges/master/pipeline.svg)](https://gitlab.com/sandervankasteel/gitlab-performance-reporter/commits/master)

## Introduction
This project was born out necessity for myself. Gitlab allows you to run Browser Performance Testing by using sitespeed.io docker container and relay that information to the MR request (https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html).
However, there is one catch, Gitlab will only show this information if you are a `premium` or `silver` subscriber. 

The aim of this project to provide similar functionality without being a Gitlab `Premium` or `Silver` subscriber. 

Currently it does this by comparing the `performance.json` (if present) of the source and target branch and adding the result of the comparison comment in the MR. 
For an example of this, see the screenshots section of this README.md

Please note, currently this project only supports the output of sitespeed.io and is in **no way** affiliated with Gitlab, GitLab Inc or it's subsidiaries.
## Usage
This usage guide, assumes you have implemented the `performance` step as mentioned in the 'Browser Performance Testing' article from Gitlab.

1. In order to use this, you will need an 'Personal Access Token' with the scope `api`. You can give it any name you want.
[More info on creating Personal Access Tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
![Personal Access Token](docs/img/gitlab-access-token.png)

2. Once you have done that, you need to add the token to your 'CI/CD Settings' as a variable. In this example I will use `GITLAB_ACCESS_TOKEN`.

**!! Don't forget to set the 'masked' the option !!**

This token allows anybody to do anything on Gitlab on your behalf and you don't want it visible to the whole world ;-)
 
![CI/CD Settings](docs/img/ci_cd-variables.png)

3. Update .gitlab-ci.yml to use this Docker image
If you named your CI/CD Settings variable differently, please use that variable in your `.gitlab-ci.yaml`
```yaml
performance_reporter:
  stage: performance_report
  image: sandervankasteel/gitlab-performance-reporter:1.0.1
  dependencies:
    - performance
  script:
    - python /app/src/main.py --token $GITLAB_ACCESS_TOKEN
```

4. Push your changes to your repository and enjoy! :)

## Configuration options

- `source`
The source branch to compare against. By default it uses the current branch in your Gitlab CI pipeline. 
In normal use I can not think of any use cases, but available for development purposes.

- `project_id`
The Gitlab project ID. By default the uses the project id as availabe in the env variables in your Gitlab CI pipeline.
In normal use I can not think of any use cases, but available for development purposes.  

- `token`
Your Gitlab Personal Access Token. 

- `job`
The name of the job in which the "performance.json" is being generated. Defaults to "performance"

- `template`
The filename of the template to use (ex. classic.md), it defaults back to `classic.md` because that's currently the only one available 

## Screenshot(s):

![Example 1](docs/img/example-1.png)
 
## Possible upcoming functionality
- Integration with Lighthouse-ci (https://github.com/GoogleChrome/lighthouse-ci/blob/master/docs/getting-started.md) and possible others
- Configurable non-zero exit codes based on the percentage of failed metrics
- Allow external markdown templates to be used

## Support
I've been dogfooding this project, but should you run into any issues you can create a Gitlab Issue or shoot me a DM / mention on [Twitter](https://twitter.com/Hertog6)

